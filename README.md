# Python Web App - Kubernetes - HELM

![](schema.png)

This project includes a python web application and its deployment to the kubernetes environment with Helm chart.

# How To Use?
You can deploy this project in your kubernetes environment with Helm chart. You can use the commands below.

        helm repo add <custom_name> https://baranarda.github.io/python-app-helm/
        helm repo update
        helm install <custom_name>/python-web-app --generate-name --values=<your_custom_values_yaml>

# Python Web application

Libraries:
 - Flask
 - SQL Alchemy
 - Postgres(psycopg2)

This application contains two functions.

GET http://application.domain.com/?number=<X> : When the client sends a get request to this URL, it returns the square of the number X.
  - Data type of X is float
  - Return X*X
  - Return Http Response 200

GET http://application.domain.com/blacklisted : When the client sends a GET request to this URL, the client IP is added to the 'blacklist' table in the database and cannot send requests to other endpoints. The information that this client is blocked is sent to 'test@domain.com' by e-mail.
  - Return Http Response 444
  - Return IP blocked
  - Insert record to DB Table(blacklist)

Example Screenshots:

GET - http://application.domain.com/?number=X 
![](number_response.png)

GET - http://application.domain.com/blacklisted
![](blacklist.png)

# Postgres Primary-Read Replica

Postgres database is used on the basis of primary-read replica. It contains only the 'blacklist' table. In Deployment part, DB configuration files are defined as kubernetes configmap file. 

Primary DB: postgresql.conf,pg_hba.conf,initial.sql
Read Replica DB: postgresql.conf,recovery.conf

Table: blacklist
Columns: id(primary key,integer),path(varchar),ipaddress(varchar),timestamp(dateTime)
Replication User: repuser

# Deployment(Kubernetes,HELM Charts)

It has been deployed as 3 subcharts in the deployment part. These; Postgres Primary DB is Postgres Read Replica DB and Python Web App.

Postgres Primary DB Components:
  - Deployment.yaml : Application deployment files. It is deployed as 'Deployment' and required Volume Mounts. Environment variables come from values.yaml file.
  - Configmaps.yaml : Postgres configurations are kept in kubernetes as configmaps. Configuration files are under the 'configs' folder and these files are used during deployment.(postgres.conf,pg_hba.conf,initial.sql)
  - PersistentVolume.yaml : Postgres required a persistent volume and it defines here.
  - PersistentVolumeClaim.yaml : Persistent volumes are claimed with this file. Some variables come from values.yaml.
  - Secrets.yaml : DB Password and SMTP server password are kept in kubernetes as secret. Deployment yaml are using these secrets.
  - Service.yaml : A service is created for the Primary DB for other applications to access the DB. 'Cluster IP' is recommended as a type.

Postgres Read Replica DB Components:
 - Deployment.yaml : Application deployment files. It is deployed as 'Deployment' and required Volume Mounts. Environment variables come from values.yaml file. Deployment includes one init container. With this container, it is checked whether the Primary DB is ready or not.
 - Configmaps.yaml : Postgres configurations are kept in kubernetes as configmaps. Configuration files are under the 'configs-sb' folder and these files are used during deployment.(postgres.conf,recovery.conf)
 - PersistentVolume.yaml : Postgres required a persistent volume and it defines here.(It should be different from Primary DB volume)
 - PersistentVolumeClaim.yaml : Persistent volumes are claimed with this file. Some variables come from values.yaml.
 - Service.yaml : A service is created for the Read Replica DB for users to access the DB. 'NodePort' is recommended as a type for accessing DB data for DB users.

Python Web App Components:
 - Deployment.yaml : Application deployment files. It is deployed as 'Deployment' and not required Volume Mounts. This is not stateful app.
 - Service.yaml : A service is created for the Python Web App for users to access the App. 'LoadBalancer' is recommended as a type for accessing App. If ingress is used, this service is linked to ingress.
 - Ingress.yaml : The Python Web App service is specified in ingrest. It is for external access and domain name definition is made here.

# Values.yaml

Values.yaml file basically consists of global, primarydb, readreplica and pythonapp parts. Make volume mount, service, image and ingress settings with the Values.yaml file.

Important: You must define mount path in Values.yaml

Folder Tree for HELM subchart Implementation

![](tree.png)
