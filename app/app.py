from datetime import datetime
import ipaddress
import os
import string
from flask_sqlalchemy import SQLAlchemy
import socket
import flask
from flask import jsonify, request
from sqlalchemy import String
from flask_mail import Mail, Message

database_uri = 'postgresql+psycopg2://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(
    dbuser=os.environ['DBUSER'],
    dbpass=os.environ['DBPASS'],
    dbhost=os.environ['DBHOST'],
    dbname=os.environ['DBNAME']
)

app = flask.Flask(__name__)
app.config.update(
    SQLALCHEMY_DATABASE_URI=database_uri,
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)
results = []
app.config['MAIL_SERVER']=os.environ['MAIL_SERVER']
app.config['MAIL_PORT'] =os.environ['MAIL_PORT']
app.config['MAIL_USERNAME'] =os.environ['MAIL_USERNAME']
app.config['MAIL_PASSWORD'] =os.environ['MAIL_PASSWORD']
app.config['MAIL_USE_TLS'] =os.environ['MAIL_USE_TLS']
app.config['MAIL_USE_SSL'] =os.environ['MAIL_USE_SSL']
mail = Mail(app)
# initialize the database connection
db = SQLAlchemy(app)


class APIError(Exception):
    pass

class APIAuthError(APIError):
  code = 444
  description = "BlackList Error"

@app.before_request
def before_request():
    from models import Guest
    visitorIp = socket.gethostbyname(socket.gethostname())    
    if Guest.query.filter_by(ipaddress=visitorIp).first() :
           raise APIAuthError()

@app.route("/", methods=["GET"])
def home():
    
    number = request.args.get('number')
    try:
        
        result = float(number) * float(number)
        response = {"given_number": float(number), "result": result}
        results.append(response)
        return jsonify(response), 200
    except Exception as e:
        return dict(error="Calculation Error", exception=str(e), message="Ensure you provide number!"), 400

@app.route("/blacklisted", methods=["GET"])
def blackListed():
    from models import Guest
    
    #Insert Block IP to DB
    ipaddress = socket.gethostbyname(socket.gethostname())
    path = request.base_url
    timestamp = datetime.now()
    db.create_all()
    db.session.commit()
    guest = Guest(path, ipaddress, timestamp)
    db.session.add(guest)
    db.session.commit()
    
    #Send Mail to 'test@domain.com'
    try:
       msg = Message('Blocked IP', sender = 'admin@domain.com', recipients = ['test@domain.com'])
       msg.body = "IP Blocked: "+ipaddress+""
       mail.send(msg)
    except Exception as e:
       print(e)
    #Return 444 Error Page
    raise APIAuthError() 

@app.errorhandler(404)
def page_not_found(error):
    return dict(response="This page does not exist"), 404

@app.errorhandler(APIError)
def handle_exception(err):
    """Return custom JSON when APIError or its children are raised"""
    response = {"error": err.description, "message": socket.gethostbyname(socket.gethostname()) +" IP is blocked", "response_code": err.code}
    return jsonify(response), err.code

if __name__ == "__main__":
    app.run(host="0.0.0.0",port='4455')